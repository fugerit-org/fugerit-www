package org.fugerit.java.www;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.fugerit.java.doc.base.config.DocConfig;
import org.fugerit.java.doc.base.config.DocInput;
import org.fugerit.java.doc.base.config.DocOutput;
import org.fugerit.java.doc.base.facade.DocFacade;
import org.fugerit.java.doc.base.facade.DocHandlerFacade;
import org.fugerit.java.doc.base.model.DocBase;
import org.fugerit.java.www.config.FugeritDocConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MainGen {

	private static final Logger logger = LoggerFactory.getLogger( MainGen.class );
	
	public static final String DEST = "target/site";
	
	private static final String BASE_PATH = "src/main/resources/free_marker/html";
	
	private static void handle( DocHandlerFacade facade, String relPath, File dest ) throws Exception {
		File sourceFile = new File( BASE_PATH, relPath );
		String type = DocConfig.TYPE_HTML_FRAGMENT;
		File destFile = new File( dest, relPath.replace( ".xml" , "."+type ) );
		destFile.getParentFile().mkdirs();
		try ( FileInputStream fis = new FileInputStream( sourceFile );
				FileOutputStream fos = new FileOutputStream( destFile ) ) {
			DocBase docBase = DocFacade.parse( fis );
			
			DocInput input = DocInput.newInput( type , docBase );
			DocOutput output = DocOutput.newOutput( fos );
			facade.handle( input, output );
			logger.info( "file written : "+destFile.getCanonicalPath() );
		}
	}
	
	public static void main( String[] args ) {
		try {
			File file = new File( DEST );
			logger.info( "DEST PATH : "+file.getCanonicalPath()+" create? "+file.mkdirs() );
			DocHandlerFacade facade = FugeritDocConfig.getFacade();
			handle(facade, "index.xml", file);
		} catch (Exception e) {
			logger.error( "Fatal error! "+e, e );
		}
	}
	
}
