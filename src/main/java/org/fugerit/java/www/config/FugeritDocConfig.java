package org.fugerit.java.www.config;

import org.fugerit.java.doc.base.facade.DocHandlerFacade;
import org.fugerit.java.doc.base.facade.DocHandlerFactory;

public class FugeritDocConfig  {

	private static DocHandlerFactory initFactory() {
		try {
			return DocHandlerFactory.newInstance( "cl://config/doc-handler-config.xml" );
		} catch (Exception e) {
			throw new RuntimeException( e );
		}
	}
	
	private final static DocHandlerFactory FACTORY = initFactory();

	public static DocHandlerFactory getFactory() {
		return FACTORY;
	}
	
	private static DocHandlerFacade FACADE = FACTORY.get( "default" );
	
	public static DocHandlerFacade getFacade() {
		return FACADE;
	}
	
}
